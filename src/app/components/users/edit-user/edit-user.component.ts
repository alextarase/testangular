import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from './../../../services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
})
export class EditUserComponent implements OnInit {
  user: User = {
    id: '',
    first_name: '',
    last_name: '',
    email: '',
    age: 0,
    gender: '',
  };
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe({
      next: (params) => {
        const id = params.get('id');
        if (id) {
          this.userService.getUser(id).subscribe({
            next: (response) => {
              this.user = response;
            },
          });
        }
      },
    });
  }

  updateUser() {
    this.userService.updateUser(this.user).subscribe({
      next: () => {
        this.router.navigate(['users']);
      },
    });
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id).subscribe({
      next: () => {
        this.router.navigate(['users']);
      },
    });
  }
}
