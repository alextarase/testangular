import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from './../../../services/user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
})
export class CreateUserComponent implements OnInit {
  addUserRequerst: User = {
    id: '',
    first_name: '',
    last_name: '',
    email: '',
    age: 0,
    gender: '',
  };
  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  addUser() {
    this.userService.addUser(this.addUserRequerst).subscribe({
      next: () => {
        this.router.navigate(['users']);
      },
      error: (error) => {
        console.log(error);
      },
    });
  }
}
