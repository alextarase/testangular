import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  //ОБРАТИ ВНИМАНИЕ! Верный ли адрес

  API_URL = 'http://localhost:3000/users';

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.API_URL);
  }

  addUser(addUserRequerst: User): Observable<User> {
    addUserRequerst.id = uuidv4();
    return this.http.post<User>(this.API_URL, addUserRequerst);
  }

  getUser(id: string): Observable<User> {
    return this.http.get<User>(this.API_URL + '/' + id);
  }

  updateUser(user: User): Observable<User> {
    return this.http.patch<User>(this.API_URL + '/' + user.id, user);
  }

  deleteUser(id: string): Observable<User> {
    return this.http.delete<User>(this.API_URL + '/' + id);
  }
}
